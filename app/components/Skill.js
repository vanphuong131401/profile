import React from "react";

const Skill = () => {
  const langIcons = [
    "fa-html5",
    "fa-css3-alt",
    "fa-square-js",
    "fa-react",
    "fa-vuejs",
    "fa-golang",
    "fa-sass",
    "fa-less",
    "fa-php",
    "fa-wordpress",
    "fa-joomla",
  ];
  const workFlows = [
    "Mobile-First, Responsive Design",
    "Cross Browser Testing & Debugging",
    "Cross Functional Teams",
    "Agile Development & Scrum",
  ];
  return (
    <section className="resume-section" id="skill">
      <div className="resume-section-content">
        <h2 className={"mb-5"}>Skill</h2>
        <div className="subheading mb-3">PROGRAMMING LANGUAGES & TOOLS</div>
        <ul className="list-inline dev-icons flex justify-between pr-4">
          {langIcons.map((lan, i) => (
            <li key={i} className="list-inline-item">
              <i className={"fa-brands " + lan}></i>
            </li>
          ))}
        </ul>
        <div className="subheading mb-3">Workflow</div>
        <ul className="fa-ul mb-0">
          {workFlows.map((w, i) => (
            <li key={i}>
              <i className="fa-sharp fa-solid fa-check mr-2"></i>
              {w}
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
};

export default Skill;

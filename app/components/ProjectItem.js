"use client";
import { Card, Image, Space, Tag, Typography } from "antd";
import Meta from "antd/es/card/Meta";
import Link from "next/link";
import React from "react";

const ProjectItem = ({ project }) => {
  if (!project) {
    return;
  }
  const colorTags = [
    "magenta",
    "red",
    "volcano",
    "orange",
    "gold",
    "lime",
    "green",
    "cyan",
    "blue",
    "geekblue",
    "purple",
  ];
  return (
    <Card
      cover={<Image preview={true} alt="projects-cover" src={project.image} />}
    >
      <Meta
        title={<Typography.Title level={4}>{project.name}</Typography.Title>}
        description={project.description}
      />
      <div className="pt-3">
        Tags:
        <Space className="pl-3" size={[0, 8]} wrap>
          {project.tags.map((t, i) => (
            <Tag key={i} color={colorTags[_.random(10)]}>
              {t}
            </Tag>
          ))}
        </Space>
      </div>
      <div className="pt-3">
        Site address: <Link href={project.link}>{project.link}</Link>
      </div>
      <Space className="pt-3" size={10} wrap>
        <div>
          <span className="mr-2 font-semibold">Team size:</span>
          {project.teamSize + ", "}
        </div>
        <div>
          <span className="mr-2 font-semibold">Position:</span>
          {project.position}
        </div>
      </Space>
      <p className="pt-3">{project.teamDes}</p>
    </Card>
  );
};

export default ProjectItem;

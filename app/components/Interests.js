"use client";
import React from "react";

const Interests = () => {
  return (
    <section className="resume-section" id="interests">
      <div className="resume-section-content">
        <h2 className="mb-5">Interests</h2>
        <p>
          Apart from being a web developer, I enjoy most of my time being
          indoors, reading books, listen to musics, play E-sports.
        </p>
        <p className="mb-0">
          Besides these hobbies, I also cherish team-building activities and
          indulge in sports-related travel, particularly running. Participating
          in these events fosters strong camaraderie and enhances my physical
          fitness. Embracing nature&apos;s beauty and overcoming challenges in
          new locations brings me immense joy.
        </p>
      </div>
    </section>
  );
};

export default Interests;

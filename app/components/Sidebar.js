"use client";
import { Anchor, Avatar, Image, Menu, Skeleton } from "antd";
import React, { useEffect, useState } from "react";
import Link from "next/link";

const Sidebar = () => {
  const [activeKeys, setActiveKeys] = useState([]);
  const navItems = [
    "about",
    "experience",
    "education",
    "skill",
    "interests",
    "projects",
  ];
  const items = navItems.map((title) => {
    return {
      key: title,
      label: (
        <Link
          className="nav-link"
          href={"#" + title}
          onClick={(e) => {
            e.preventDefault();
            const hash = e.target.hash.slice(1);
            const originalElement = document.getElementById(hash);
            if (originalElement) {
              originalElement.scrollIntoView({ behavior: "smooth" });
              setActiveKeys([hash]);
            }
          }}
        >
          {title}
        </Link>
      ),
    };
  });

  const anchorItems = navItems.map((title) => {
    return {
      key: title,
      href: "#" + title,
      title: title,
    };
  });

  //functions
  const menuSelected = ({ item, domEvent }) => {
    // e.preventDefault();
  };

  //hooks

  return (
    <nav
      className="navbar navbar-expand-lg navbar-dark bg-primary fixed-top"
      id="sideNav"
    >
      <Avatar
        className="navbar-avata "
        size={175}
        src={
          <Image
            className="img-fluid img-profile rounded-circle mx-auto mb-2"
            src="/images/avt.jpg"
            alt="profile"
            preview={true}
          />
        }
      />
      <div className="navbar-collapse">
        <Anchor
          className="hidden"
          onChange={(e) => {
            setActiveKeys([e.replace("#", "")]);
          }}
          items={anchorItems}
        />
        <Menu
          onClick={menuSelected}
          selectedKeys={activeKeys || []}
          style={{
            backgroundColor: "transparent",
          }}
          className="navbar-nav pt-4"
          items={items}
          mode={"vertical"}
          theme="dark"
        />
      </div>
    </nav>
  );
};

export default Sidebar;

import React from "react";

const Education = () => {
  const academicDeg = "Bachelor's degree in Information Technology";
  const college = "University of Technology and Education";
  return (
    <section className="resume-section" id="education">
      <div className="resume-section-content">
        <h2 className={"mb-5"}>Education</h2>
        <div className="d-flex flex-column flex-md-row justify-content-between mb-5">
          <div className="flex-grow-1">
            <h3 className="mb-0">{college}</h3>
            <div className="subheading mb-3">{academicDeg}</div>
            <div>Major: Infomation Technology</div>
            <p>GPA: 3.56</p>
          </div>
          <div className="flex-shrink-0">
            <span className="text-primary">September 2016 - Feburary 2019</span>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Education;

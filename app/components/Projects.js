"use client";
import { Card, Image, Skeleton, Space, Tag } from "antd";
import Meta from "antd/es/card/Meta";
import _ from "lodash";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import ProjectItem from "./ProjectItem";

// {
//   "name": "Haut-Lac School",
//   "description": "Technology: WordPress, PHP, Javascript, CSS. Haut-Lac International Bilingual School is a coeducational international school for ages 3 to 18 years located in Saint-Légier-La Chiésaz, in the canton of Vaud, Switzerland.",
//   "category": "CMS",
//   "tags": ["WordPress", "PHP", "Javascript"],
//   "link": "https://www.haut-lac.ch/",
//   "image": "/images/hautlac_screen.png",
//   "teamSize": 5,
//   "position": "Team member",
//   "teamDes": "Developing site with PHP, CSS, builder page content."
// },

const Projects = () => {
  const [projects, setProjects] = useState();
  const projectsJson = require("../../public/projectsData.json");

  useEffect(() => {
    if (projectsJson && projectsJson.length) {
      setProjects(projectsJson);
    }
  }, [projectsJson]);

  if (!projects) {
    return <Skeleton />;
  }

  return (
    <section className="resume-section" id="projects">
      <div className="resume-section-content">
        <h2 className="mb-4">Personal projects</h2>
        <h3 className="mb-3">CMS</h3>
        <div className="projects-grid">
          {projects.map((project, i) => {
            return <ProjectItem key={i} project={project} />;
          })}
        </div>
      </div>
    </section>
  );
};

export default Projects;

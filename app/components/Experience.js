import React from "react";

const Experience = () => {
  const ex1 =
    "Approaching CMS WordPress, Joomla. Building custom e-commerce websites according to client requirements. Maintaining and addressing customer inquiries about specific details within the website.";
  const ex2 =
    "Analyzing, constructing systems, and developing internal Web Apps for the company. Researching new technologies, techniques, and improving existing technologies that align with the company's goals.";
  return (
    <section className="resume-section" id="experience">
      <div className="resume-section-content">
        <h2 className={"mb-5"}>Experience</h2>
        <div className="d-flex flex-column flex-md-row justify-content-between mb-5">
          <div className="flex-grow-1">
            <h3 className="mb-0">Junior Web Developer</h3>
            <div className="subheading mb-3">Nifty JS</div>
            <p>{ex2}</p>
          </div>
          <div className="flex-shrink-0">
            <span className="text-primary">March 2022 - Present</span>
          </div>
        </div>
        <div className="d-flex flex-column flex-md-row justify-content-between mb-5">
          <div className="flex-grow-1">
            <h3 className="mb-0">Web Developer</h3>
            <div className="subheading mb-3">Nifty JS</div>
            <p>{ex1}</p>
          </div>
          <div className="flex-shrink-0">
            <span className="text-primary">January 2022 - March 2022</span>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Experience;

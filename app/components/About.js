import React from "react";

const About = () => {
  const des =
    "I have over 3 years of experience in the Web Developer field. I'm seeking a developer position to apply and develop programming skills such as PHP, JavaScript along with knowledge of HTML, CSS, React JS, Boostrap, JQuery.";
  const des2 =
    "I aspire to become a professional developer and contribute to building web applications and developing innovative products for the company to meet customer needs and enhance user experience.";
  const info = "Hai Chau · Da Nang";
  return (
    <section className="resume-section" id="about">
      <div className="resume-section-content">
        <h2 className={"mb-1"}>
          Nguyen <span className="text-primary">Van Phuong</span>
        </h2>
        <div className="subheading mb-5 flex">
          {info + " · "}
          <a className="mx-1" href="tel:0708059303">
            0708059303
          </a>
          {"·"}
          <a className="mx-1" href="mailto:vanphuong131401@gmail.com">
            vanphuong131401@gmail.com
          </a>
        </div>
        <p className="lead mb-3">{des}</p>
        <p className="lead mb-5">{des2}</p>
        <div className="social-icons">
          <a
            className="social-icon"
            target="_blank"
            href="https://www.linkedin.com/in/ray-david-3302281b9/"
          >
            <i className="fa-brands fa-linkedin-in"></i>
          </a>
          <a
            className="social-icon"
            target="_blank"
            href="https://gitlab.com/vanphuong131401"
          >
            <i className="fa-brands fa-gitlab"></i>
          </a>
          <a
            className="social-icon"
            target="_blank"
            href="https://twitter.com/raydavi19898532"
          >
            <i className="fa-brands fa-twitter"></i>
          </a>
          <a
            target="_blank"
            className="social-icon"
            href="https://www.facebook.com/nguyenvan.phuong.52643/"
          >
            <i className="fa-brands fa-facebook-f"></i>
          </a>
        </div>
      </div>
    </section>
  );
};

export default About;

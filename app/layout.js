import "../styles/globals.css";
import "../styles/boostrap.css";
import { Mulish, Saira_Extra_Condensed } from "next/font/google";
import Script from "next/script";
import Sidebar from "./components/Sidebar";

export const saira = Saira_Extra_Condensed({
  subsets: ["latin"],
  weight: ["300", "400", "500", "600", "700"],
  variable: "--font-saira",
});
export const muli = Mulish({
  subsets: ["latin"],
  weight: ["300", "400", "500", "600", "700"],
  variable: "--font-muli",
});

export const metadata = {
  title: "Nguyen Van Phuong",
  description: "My profile.",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <Script src="https://kit.fontawesome.com/9e4508502b.js"></Script>
      <body className={saira.variable + " " + muli.className}>
        <Sidebar />
        <main className="container-fluid p-0">{children}</main>
      </body>
    </html>
  );
}

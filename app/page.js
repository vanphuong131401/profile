import About from "./components/About";
import Education from "./components/Education";
import Experience from "./components/Experience";
import Interests from "./components/Interests";
import Projects from "./components/Projects";
import Skill from "./components/Skill";

export default function Home() {
  //variables

  return (
    <div className="container-fluid p-0">
      <About />
      <Experience />
      <Education />
      <Skill />
      <Interests />
      <Projects />
    </div>
  );
}
